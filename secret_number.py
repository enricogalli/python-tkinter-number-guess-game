import random


class SecretNumber:
    def __init__(self, level, numbers_per_level):

        self.numbers_per_level = numbers_per_level
        self.level = level
        self.max_number = self.numbers_per_level * self.level
        self.secret_number = 0
        self.newNumber(self.level)

    def newNumber(self, level):
        self.level = level
        self.max_number = self.numbers_per_level * self.level
        self.secret_number = random.randint(1, self.max_number)
