from appdirs import user_data_dir
from os import path, mkdir

DOMAIN: str = "egw.it"
BRAND_DIR = user_data_dir(f"{DOMAIN}")
PROJECT: str = "guess-a-number"
PROJECT_DIR = user_data_dir(f"{DOMAIN}/{PROJECT}")

if not path.exists(BRAND_DIR):
    mkdir(BRAND_DIR)

if not path.exists(PROJECT_DIR):
    mkdir(PROJECT_DIR)

print("DATA DIR:", PROJECT_DIR)




