import tkinter as tk
from guessgame_terminal import GuessGame
from guessgame_tk_screen_game import GameScreen
from guessgame_tk_screen_main import MainScreen
from guessgame_tk_screen_gameover import GameOverScreen


class GuessGameTk(GuessGame):
    def __init__(self, root):
        super().__init__()

        self.root = root

        # GAME SCREEN
        self.screen_game = GameScreen(self, bg='#862633')

        # GAME OVER SCREEN
        self.screen_gameover = GameOverScreen(self, bg='black')

        # MAIN SCREEN
        self.screen_main = MainScreen(self, bg='#862633')

        # Display Top Players on MAIN SCREEN
        self.displayTopPlayers()

    def play(self):

        self.restart()
        self.screen_game.updateLevelLabel()
        self.screen_game.updateGoalLabel()

        # slide up the game area
        self.screen_game.lift()
        self.screen_game.number_input_fld.focus()

    def playAgain(self, play_again=True):
        if play_again:
            self.play()
        else:
            self.screen_main.lift()
            self.displayTopPlayers()

    def finalScore(self):

        self.screen_gameover.clearSummaryArea()

        # FINAL SCORE
        lbl = tk.Label(
            self.screen_gameover.frame_summary, text='YOUR FINAL SCORE IS', bg='black', fg='#cccccc',
            font=('helvetica', 8, 'bold')
        )
        lbl.pack(side='top', pady=(10, 0))

        lbl = tk.Label(
            self.screen_gameover.frame_summary, text=self.score.current_score, bg='black', fg='#cccccc',
            font=('helvetica', 20, 'bold')
        )
        lbl.pack(side='top', pady=(0, 10))

        # CHECK SCORE RANKING
        this_score = self.score.checkScore()

        if this_score == "NEWSCORE":
            lbl = tk.Label(
                self.screen_gameover.frame_summary, text='CONGRATULATION, GREAT SCORE', bg='black', fg='#cccccc',
                font=('helvetica', 8, 'bold')
            )
            lbl.pack(side='top', pady=(30, 0))
            store_score = True

        elif this_score == "MAXSCORE":
            lbl = tk.Label(
                self.screen_gameover.frame_summary, text='AMAZING, NEW HISCORE ', bg='black', fg='#cccccc',
                font=('helvetica', 8, 'bold')
            )
            lbl.pack(side='top', pady=(30, 0))
            store_score = True

        else:
            store_score = False
            self.screen_gameover.displayTopPlayersOver()

        if store_score:

            lbl = tk.Label(
                self.screen_gameover.frame_summary, text='TYPE YOUR NAME AND HIT ENTER ', bg='black', fg='#cccccc',
                font=('helvetica', 7,)
            )
            lbl.pack(side='top', pady=0)

            # reset the player name variable
            self.screen_gameover.player_name.set('')

            fld = tk.Entry(
                self.screen_gameover.frame_summary, bg='#333333', fg='#cccccc', insertbackground='white',
                border=0, highlightthickness=0,
                justify=tk.CENTER,
                font=('helvetica', 12),
                textvariable=self.screen_gameover.player_name
            )
            fld.pack(side='top', fill=tk.X, padx=40, pady=(5, 0), ipady=5)
            fld.focus()
            fld.bind('<Return>', lambda e: self.screen_gameover.storeAndShow())

    def displayTopPlayers(self):

        for child in self.screen_main.frame_top_players.winfo_children():
            child.destroy()

        self.score.hiscores.sort(reverse=True)

        # top players title
        tk.Label(
            self.screen_main.frame_top_players, bg='#862633', fg='#cccccc',
            text='GUESS A NUMBER BEST PLAYERS',
            font=('helvetica', 9, 'bold')
        ).pack(side='top', pady=(0, 40))

        # top players list
        # top_players_container = tk.Frame(
        #    self.frame_top_players, bg=self.screen_main['bg']
        # )
        # top_players_container.pack(side='top', expand=True, fill=tk.BOTH)

        player_position = 1

        for player in self.score.hiscores:
            player_score, player_name = player

            player_frame = tk.Frame(self.screen_main.frame_top_players, bg='#862633')
            player_frame.pack(side='top', fill=tk.X, padx=40)

            lbl_position = tk.Label(
                player_frame, text=player_position,
                fg='#cccccc',  bg='#862633', width=2, font=('helvetica', 9, '')
            )
            lbl_position.pack(side='left')

            lbl_score = tk.Label(
                player_frame, text=player_score,
                fg='#cccccc', bg='#862633', width=6, font=('helvetica', 9, '')
            )
            lbl_score.pack(side='left')

            lbl_name = tk.Label(
                player_frame, text=player_name, fg='#cccccc', bg='#862633', font=('helvetica', 9, '')
            )
            lbl_name.pack(side='left')

            lbl_line = tk.Frame(self.screen_main.frame_top_players, bg='#782530', height=1)
            lbl_line.pack(side='top', fill=tk.X, padx=40, pady=7)

            player_position += 1

            if player_position > 10:
                break

    def restart(self):
        super().restart()
        self.screen_game.score_area.reset()
        self.screen_game.hints_manager.reset(self.secret_number.secret_number, self.secret_number.max_number)
