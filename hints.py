class Hint:
    def __init__(self, right_answer, max_distance):

        self.right_answer = right_answer
        self.last_distance = max_distance
        self.first_attempt = True

    def get(self, guess):

        if self.first_attempt:
            hint = "First try"
            self.first_attempt = False

            distance = abs(self.right_answer - guess)

        else:

            distance = abs(self.right_answer - guess)

            if distance < self.last_distance:
                hint = "You are closer than before"
            elif distance == self.last_distance:
                hint = "You are at the SAME DISTANCE than before"
            else:
                hint = "You are NOT closer than before"

        self.last_distance = distance
        return hint

    def reset(self, right_answer, max_distance):
        self.right_answer = right_answer
        self.last_distance = max_distance
