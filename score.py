import sys
from appdata import PROJECT_DIR, path


class Score:
    def __init__(self, start_score: int):

        # highscores file. Init checks if exists or create it
        self.hiscores_file = f"{PROJECT_DIR}/hiscores"
        self.init_hiscores_file()

        self.start_score = start_score
        self.current_score = start_score

        self.wrong_guess: int = -10      # points to be removed everytime you guess it wrong
        self.right_guess: int = 500      # points to be added when you guess it right
        self.spare_attempts: int = 10    # points to add for every guess attempt you didn't use

        # hiscores
        self.hiscores: list[int] = []
        self.readScores()

    # crea il file hiscores se non esiste, all'interno nella cartella dati
    def init_hiscores_file(self) -> None:
        if not path.exists(self.hiscores_file):
            with open(self.hiscores_file, "w") as f:
                f.write("50,Clara M\n100,Jen K\n120,Ben B\n140,Kathy P\n270,Vicky S\n400,Jade D\n"
                        "10,Enrico G\n970,Evelyn D\n1120,Karl M\n20,Rudy G\n")

    def updateScore(self, value, spare_attempts=0):
        self.current_score += value
        if spare_attempts > 0:
            self.current_score += self.spare_attempts*spare_attempts

    def readScores(self, reset=False):

        if reset:
            self.hiscores = []

        try:
            base_path = sys._MEIPASS # noqa
        except AttributeError:
            base_path = './'

        filename = path.join(base_path, self.hiscores_file)

        with open(filename) as hiscores:
            for line in hiscores.read().split("\n"):
                if ',' in line:
                    data: list[str] = line.split(',')
                    score = data[0]
                    if len(data) == 2:
                        player = data[1]
                    else:
                        player = ",". join(data[1:])

                    self.hiscores.append([int(score), player])

    def checkScore(self) -> str:
        min_score, _ = min(self.hiscores)
        max_score, _ = max(self.hiscores)

        if self.current_score > max_score:
            return "MAXSCORE"

        elif len(self.hiscores) < 10 or (self.current_score > min_score):
            return "NEWSCORE"

        else:
            return "BADSCORE"

    def writeScore(self, player_name):

        try:
            base_path = sys._MEIPASS # noqa
        except AttributeError:
            base_path = './'

        filename = path.join(base_path, self.hiscores_file)

        hiscores_file = open(filename, 'a')
        hiscores_file.write(f'{self.current_score},{player_name}\n')
        hiscores_file.close()

        self.readScores(True)

    def reset(self) -> None:
        self.current_score = self.start_score
