import sys
import tkinter as tk
import guessgame_tk
import os

if __name__ == "__main__":

    # start graphical version
    root = tk.Tk(className='Guess A Number')

    filename = 'gan_icon.png'

    try:
        base_path = sys._MEIPASS # noqa
    except AttributeError:
        base_path = './'

    filename = os.path.join(base_path, filename)

    root.iconphoto(False, tk.PhotoImage(file=filename))

    root.resizable(False, False)
    root.title("Guess A Number Game")
    root.geometry('300x600')

    # Windows OS main size is a bit bigger due to font difference
    if os.name == 'nt':
        root.geometry('300x650')

    game = guessgame_tk.GuessGameTk(root)

    root.mainloop()
