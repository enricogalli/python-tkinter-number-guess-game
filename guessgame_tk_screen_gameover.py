import tkinter as tk


class GameOverScreen(tk.Frame):
    def __init__(self, target, **kwargs):
        super().__init__(target.root, **kwargs)

        self.target = target
        self.score = target.score

        self.place(x=0, y=0, relwidth=1, relheight=1)

        lbl_game_over = tk.Label(
            self, text='GAME\nOVER', bg='black', fg='#862633', font=('helvetica', 40, 'bold')
        )
        lbl_game_over.pack(side='top', pady=(50, 0))

        self.frame_summary = tk.Frame(self, bg='black')
        self.frame_summary.pack(side='top', pady=(30, 0))

        self.player_name = tk.StringVar()

        frame_play_again = tk.Frame(self, bg='black')
        frame_play_again.pack(side='bottom', pady=30, padx=40, fill=tk.X)

        lbl = tk.Label(
            frame_play_again, text='PLAY AGAIN?', bg='black', fg='#cccccc', font=('helvetica', 10, 'bold')
        )
        lbl.pack(side='top')

        btn = tk.Button(
            frame_play_again, text='YES',
            bg='green', fg='white',
            activebackground='#93e601',
            activeforeground='#333333',
            command=self.target.playAgain,
            border=0, highlightthickness=0,
            cursor='hand2'
        )
        btn.pack(side='top', fill=tk.X, pady=10)

        btn = tk.Button(
            frame_play_again, text='NO',
            bg='#862633', fg='white',
            activebackground='red',
            activeforeground='white',
            command=lambda: self.target.playAgain(False),
            border=0, highlightthickness=0,

            cursor='hand2'
        )
        btn.pack(side='top', fill=tk.X)

    def displayTopPlayersOver(self):

        self.score.hiscores.sort(reverse=True)

        # top players title
        tk.Label(
            self.frame_summary, bg='black', fg='#cccccc',
            text='TOP 5 PLAYERS',
            font=('helvetica', 9, 'bold')
        ).pack(side='top', pady=10)

        # top players list
        top_players_container = tk.Frame(
            self.frame_summary, bg='black'
        )
        top_players_container.pack(side='top', expand=True, fill=tk.BOTH)

        player_position = 1

        for player in self.score.hiscores:
            player_score, player_name = player

            player_frame = tk.Frame(top_players_container, bg='black')
            player_frame.pack(side='top', fill=tk.X, padx=40)

            lbl_position = tk.Label(
                player_frame, text=player_position,
                fg='#cccccc', bg='black', width=2, font=('helvetica', 8, '')
            )
            lbl_position.pack(side='left')

            lbl_score = tk.Label(
                player_frame, text=player_score,
                fg='#cccccc', bg='black', width=6, font=('helvetica', 8, '')
            )
            lbl_score.pack(side='left')

            lbl_name = tk.Label(
                player_frame, text=player_name, fg='#cccccc', bg='black', font=('helvetica', 8, '')
            )
            lbl_name.pack(side='left')

            lbl_line = tk.Frame(top_players_container, bg='#333333', height=1)
            lbl_line.pack(side='top', fill=tk.X, padx=40, pady=7)

            player_position += 1

            if player_position > 5:
                break

    # CLEAR SUMMARY AREA
    def clearSummaryArea(self):
        for child in self.frame_summary.winfo_children():
            child.destroy()

    def storeAndShow(self):
        self.score.writeScore(self.player_name.get())
        self.clearSummaryArea()
        self.displayTopPlayersOver()
