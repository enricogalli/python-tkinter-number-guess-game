class Attempts:
    def __init__(self, max_attempts):

        self.max_attempts = max_attempts
        self.current_attempt = 1


    def next(self):
        self.current_attempt += 1


    def attemptsLeft(self):
        return self.max_attempts - self.current_attempt


    def reset(self):
        self.current_attempt = 1
