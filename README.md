# LICENSE
All the information about this software licence is available 
inside the 'LICENSE' file provided with the app

# SUPPORT
The software is provided "as is" with no support of any kind. 

Any additional information can be found ath the page 
[https://www.egw.it/projects/guess-a-number](https://www.egw.it/projects/guess-a-number)

Issues can be submitted on [gitLab](https://gitlab.com/enricogalli/python-tkinter-number-guess-game/-/issues)
