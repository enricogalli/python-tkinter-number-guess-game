import tkinter as tk
from hints import Hint


class HintsDisplay(Hint):
    def __init__(self, target, right_answer, max_distance):
        super().__init__(right_answer, max_distance)

        self.target = target

        self.current_hints = []

        self.first_attempt = True

        self.hints_area = tk.Frame(
            self.target,
            bg='#862633'
        )
        self.hints_area.pack(side='bottom', pady=20, fill=tk.X)

    def get(self, guess):

        if self.first_attempt:
            color = 'white'
            text = f"First try"
            distance = abs(self.right_answer - guess)
            self.first_attempt = False

        else:

            distance = abs(self.right_answer - guess)

            if distance < self.last_distance:
                color = '#93e601'
                text = 'Getting closer'

            elif distance == self.last_distance:
                color = 'orange'
                text = 'At the same distance'

            else:
                color = '#ff6868'
                text = 'Getting more distant'

        self.displayHint(guess, color, text)
        self.last_distance = distance

    def displayHint(self, guess, color, text):
        hint_frame = tk.Frame(self.hints_area, bg='#862633')
        hint_frame.pack(side='bottom', pady=5, padx=40, fill=tk.X)

        lbl = tk.Label(
            hint_frame, text=guess, bg='#862633', fg=color, width=3, font=('helvetica', 9, 'bold'))
        lbl.pack(side='left')

        lbl = tk.Label(hint_frame, text=text, bg='#862633', fg='#cccccc', font=('helvetica', 9))
        lbl.pack(side='left', padx=(10, 0))

        line = tk.Frame(self.hints_area, bg='#782530', height=1)
        line.pack(side='bottom', fill=tk.X, padx=40, pady=5)

    def reset(self, right_answer, max_distance):
        super().reset(right_answer, max_distance)
        self.first_attempt = True
        self.clearHintHarea()

    def clearHintHarea(self):
        for obj in self.hints_area.winfo_children():
            obj.destroy()
