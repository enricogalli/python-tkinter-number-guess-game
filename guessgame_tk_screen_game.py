import tkinter as tk
from score_display import ScoreDisplay
from hints_display import HintsDisplay


class GameScreen(tk.Frame):
    def __init__(self, target, **kwargs):
        super().__init__(target.root, **kwargs)

        self.target = target
        self.attempts = target.attempts
        self.secret_number = target.secret_number
        self.level = target.level
        self.score = target.score

        self.number_input = tk.StringVar()
        self.current_level = tk.StringVar()
        self.max_number = tk.StringVar()

        # self.screen_game = tk.Frame(self.root, bg='#862633')

        # show
        self.score_area = ScoreDisplay(self, self.target.score)

        # goal
        level_goal_frame = tk.Frame(self, bg='#782530', height=80)
        level_goal_frame.pack(side='top', fill=tk.X, pady=(0, 20))

        level = tk.Label(
            level_goal_frame, textvariable=self.current_level,
            bg='#782530', fg='#cccccc',
            font=('helvetica', 8, 'bold')
        )
        level.pack(side='left', pady=(10, 6), padx=10)

        goal_descriprion = tk.Label(
            level_goal_frame,
            textvariable=self.max_number,
            bg='#782530', fg='#cccccc', font=('helvetica', 8)
        )
        goal_descriprion.pack(side='right', pady=(10, 6), padx=10)

        # number input
        self.number_input_fld = tk.Entry(
            self,
            bg='#862633', fg='white', insertbackground='white',
            border=0, highlightthickness=0,
            width=4,
            textvariable=self.number_input,
            font=('helvetica', 80, 'bold'),
            justify='center'
        )
        self.number_input_fld.pack(side='top')

        self.number_input_fld.focus()
        self.number_input_fld.bind('<Return>', lambda e: self.checkGuess())

        # FIRST TIP
        self.tip = tk.Label(
            self,
            bg='#862633',
            fg='#cccccc',
            text='TYPE A NUMBER AND HIT ENTER',
            font=('helvetica', 6,)
        )
        self.tip.pack(side='top', pady=20)

        # CORRECT ANSWER
        self.correct = tk.Frame(self, bg='red', width=300, height=150)

        lbl = tk.Label(self.correct, bg='#862633', fg='white', font=('helvetica', 20, 'bold'), text='CORRECT')
        lbl.pack(side='top', expand=True, fill=tk.BOTH)

        self.correct.place(x=300, y=100)
        self.correct.pack_propagate(False)

        # Hints area
        self.hints_manager = HintsDisplay(
            self, self.target.secret_number.secret_number, self.target.secret_number.max_number
        )

        self.place(x=0, y=0, relwidth=1, relheight=1)

    # check your answer
    def checkGuess(self):

        # REMOVE STARTUP TIP
        if self.tip:
            self.tip.pack_forget()
            self.tip = None

        try:
            number_input = int(self.number_input.get())
        except ValueError:
            self.number_input.set('')
            return

        self.resetInput()

        if self.secret_number.secret_number == number_input:
            # Correct answer

            self.update_idletasks()
            self.correct.place(y=self.number_input_fld.winfo_y())

            self.animateCorrect()

            self.score_area.updateScore(self.score.right_guess, self.attempts.attemptsLeft())
            self.target.nextLevel()

            self.updateLevelLabel()
            self.updateGoalLabel()

            # graphical hints reset
            self.hints_manager.reset(self.secret_number.secret_number, self.secret_number.max_number)

        # Wrong answer
        else:

            # check if there are attempts available
            if self.attempts.current_attempt < self.attempts.max_attempts:
                self.hints_manager.get(number_input)
                self.score_area.updateScore(self.score.wrong_guess)
                self.attempts.next()

            # Game over
            else:
                self.target.screen_gameover.lift()
                self.gameOver(600)

    def animateCorrect(self, start=300, lift=False):

        if not lift:
            self.correct.lift()
            lift = True

        if start > - 300:

            next_step = start-50
            next_time = 10

            if next_step == 0:
                next_time = 500

            self.correct.place(x=next_step)
            self.correct.after(next_time, lambda: self.animateCorrect(next_step, lift))

    def gameOver(self, start_point):

        end_point = start_point - 30

        if start_point > 0:
            self.target.screen_gameover.place(x=0, y=end_point)
            self.target.screen_gameover.after(10, lambda: self.gameOver(end_point))
            return

        self.target.finalScore()

    def resetInput(self):
        self.number_input.set('')

    def updateLevelLabel(self):
        self.current_level.set(f'LEVEL {str(self.level.current_level)}')

    def updateGoalLabel(self):
        self.max_number.set(f'A NUMBER BETWEEN 1 AND {self.secret_number.max_number}')
