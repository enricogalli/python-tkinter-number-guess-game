import guessgame_terminal


if __name__ == "__main__":

    # start terminal version
    game = guessgame_terminal.GuessGame()
    game.start()

    while not game.game_over:
        game.play()
