import tkinter as tk


class ScoreDisplay(tk.Frame):
    def __init__(self, target, score, **kwargs):
        super().__init__(target, **kwargs)

        self.target = target
        self.score = score
        self.current_score = tk.StringVar(value=self.score.current_score)

        self['bg'] = 'black'
        self.pack(side='top', fill=tk.X)

        # BEST SCORE SECTION
        self.score.hiscores.sort(reverse=True)
        player_score, _ = self.score.hiscores[0]
        self.best_score = player_score
        self.best_score_lbl = tk.StringVar()
        self.updateBestScore()

        # YOUR SCORE SECTION
        self.your_score_label = tk.Label(
            self, bg='black', fg='#cccccc', textvariable=self.best_score_lbl,
            font=('helvetica', 8, '')
        )
        self.your_score_label.place(x=7, y=7)

        self.your_score_label = tk.Label(
            self, bg='black', fg='#ffffff', text='YOUR SCORE', font=('helvetica', 6, 'bold')
        )
        self.your_score_label.pack(anchor=tk.NE, padx=10, pady=(5, 0))

        self.current_score_label = tk.Label(
            self, bg='black', fg='#999999', text=self.score.current_score, textvariable=self.current_score,
            font=('helvetica', 20, 'bold')
        )
        self.current_score_label.pack(anchor=tk.SE, padx=10, pady=(0, 5))

        # Energy
        self.energy_frame = tk.Frame(self.target, bg='#333333')
        self.energy_frame.pack(side='top', fill=tk.X)

        self.energy_bar = tk.Frame(self.energy_frame, bg='#d2ff1d', height=4, width=300)
        self.energy_bar.pack(side='left')

    def updateBestScore(self):
        if self.score.current_score > self.best_score:
            self.best_score_lbl.set(f'BEST SCORE: {self.score.current_score}')
        else:
            self.best_score_lbl.set(f'BEST SCORE: {self.best_score}')

    # method to animate the score
    def updateScore(self, score, spare_attempts=0):
        self.score.updateScore(score, spare_attempts)
        self.updateBestScore()
        self.animateScore(int(self.current_score.get()), self.score.current_score)

        if score < 0:
            self.animateEnergy(self.energy_bar['width'], self.energy_bar['width'] - 30)
        else:
            self.animateEnergy(self.energy_bar['width'], 300)

    # method to animate the energy bar
    def animateScore(self, current_score, new_score):

        step_size = -2

        if current_score != new_score:

            if new_score > current_score:
                step_size *= -5

            next_step = current_score + step_size

            self.current_score.set(next_step)
            self.after(10, lambda: self.animateScore(next_step, new_score))

    def animateEnergy(self, current_size, new_size):

        step_size = -2

        if current_size != new_size:

            if new_size > current_size:
                step_size *= -10

            next_step = current_size + step_size

            self.energy_bar['width'] = next_step
            self.energy_bar.after(10, lambda: self.animateEnergy(next_step, new_size))

    def reset(self):
        self.energy_bar['width'] = 300
        self.current_score.set(self.score.current_score)
