import tkinter as tk
from PIL import ImageTk, Image
import sys
import os


class MainScreen(tk.Frame):
    def __init__(self, target, **kwargs):
        super().__init__(target.root, **kwargs)

        self.target = target

        # logo picture

        filename = 'logo.png'

        try:
            base_path = sys._MEIPASS # noqa
        except AttributeError:
            base_path = './'

        filename = os.path.join(base_path, filename)

        img = ImageTk.PhotoImage(Image.open(filename))
        label = tk.Label(
            self, image=img, bg='#862633'
        )
        label.pack(side='top', pady=40, padx=0, fill=tk.X)
        label.image = img

        self.frame_top_players = tk.Frame(self, bg='#862633')
        self.frame_top_players.pack(side='top', fill=tk.X)

        # Start game button
        self.start_btn = tk.Button(
            self,
            text='START A NEW GAME',
            bg='black',
            fg='white',
            height=3,
            font=('helvetica', 8, ''),
            border=0,
            highlightthickness=0,
            activebackground='#d2ff1d',
            cursor='hand2',
            command=self.target.play
        )
        self.start_btn.pack(side='bottom', fill=tk.X, padx=20, pady=20)

        if os.name == 'nt':
            self.start_btn.bind(
                '<Enter>', lambda e: self.winBtnColor(self.start_btn['activebackground'], self.start_btn['bg'])
            )
            self.start_btn.bind('<Leave>', lambda e: self.winBtnColor(self.start_btn['fg'], 'white'))

        self.place(x=0, y=0, relwidth=1, relheight=1)

    def winBtnColor(self, bg, fg):
        self.start_btn['bg'] = bg
        self.start_btn['fg'] = fg
    
