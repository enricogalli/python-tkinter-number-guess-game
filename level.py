class Level:
    def __init__(self):

        self.start_level = 1
        self.current_level = self.start_level

    def nextLevel(self):
        self.current_level += 1
        return self.current_level

    def reset(self):
        self.current_level = self.start_level
