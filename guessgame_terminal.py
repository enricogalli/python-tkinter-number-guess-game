import level
import secret_number
import attempts
import hints
import score


class GuessGame:
    def __init__(self):

        self.level = level.Level()
        self.secret_number = secret_number.SecretNumber(self.level.current_level, 10)
        self.attempts = attempts.Attempts(10)
        self.hints = hints.Hint(self.secret_number.secret_number, self.secret_number.max_number)
        self.score = score.Score(100)

        self.game_over = False

    def start(self):
        print('Welcome to a Number Guess Game\n')
        print('Guess a number')
        print(f'You start by guessing a number between 1 and {self.secret_number.max_number} '
              f'in maximum {self.attempts.max_attempts} attempts')
        print(f'Every level increases difficulty by adding {self.secret_number.numbers_per_level} additional numbers\n')

        input('Press a key to start')
        print('\n\n\n')

    def play(self):
        while self.attempts.current_attempt <= self.attempts.max_attempts:

            print(f'[ Current score: {self.score.current_score} ]\n'
                  f'Level {self.level.current_level} '
                  f'Attempt {self.attempts.current_attempt} of {self.attempts.max_attempts}')
            guess = input(f'Guess a number between 1 and {self.secret_number.max_number} : ')

            try:
                guess = int(guess)

                if guess == self.secret_number.secret_number:

                    print("\n\nAMAZING! Your guess is correct")
                    self.score.updateScore(self.score.right_guess, self.attempts.attemptsLeft())
                    print(f'Level Complete! [ Your score {self.score.current_score} ]\n\n')
                    self.nextLevel()
                    break

                else:
                    if self.attempts.current_attempt < self.attempts.max_attempts:
                        print(f'\n{self.hints.get(guess)}')
                        print('Better luck next time\n')
                        self.score.updateScore(self.score.wrong_guess)
                    else:
                        print('\n\nGAME OVER')
                        self.finalScore()
                        self.playAgain()
                        break

                    self.attempts.next()
            except ValueError:
                print('\n\nYou did not enter a valid number\n\n')

    def playAgain(self):
        play_again = input("Do you want to play again? (Y/n) ")
        if play_again == 'Y':
            self.game_over = False
            self.restart()
            print('\n\n\n\n\n\n\n\n\n\n\n\n')
            return

        self.game_over = True

    def finalScore(self):
        print(f'\n\n[ FINAL SCORE: {self.score.current_score}]\n\n')
        this_score = self.score.checkScore()
        if this_score == "NEWSCORE":
            print("\n\nCongratulation! You did a new good score")
            store_score = True

        elif this_score == "MAXSCORE":
            print("\n\nAMAZING! You are the best player!")
            store_score = True
        else:
            store_score = False

        if store_score:
            print('Your name will be recorded in our top 10 board.')
            player_name = input('Write your name: ')
            self.score.writeScore(player_name)

            self.displayTopPlayers()

    def displayTopPlayers(self):
        print('\n\n')
        print('GUESS A NUMBER BEST PLAYERS')

        self.score.hiscores.sort(reverse=True)

        player_position = 1

        for player in self.score.hiscores:
            player_score, player_name = player
            print(f'{player_position}.\t[{player_score}]\t{player_name}')
            player_position += 1

            if player_position > 10:
                break

        print('\n\n')

    def nextLevel(self):
        self.attempts.reset()
        self.secret_number.newNumber(self.level.nextLevel())
        self.hints.reset(self.secret_number.secret_number, self.secret_number.max_number)

    def restart(self):
        self.attempts.reset()
        self.level.reset()
        self.score.reset()
        self.secret_number.newNumber(self.level.current_level)
        self.hints.reset(self.secret_number.secret_number, self.secret_number.max_number)
